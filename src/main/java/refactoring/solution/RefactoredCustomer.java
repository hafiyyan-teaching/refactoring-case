package refactoring.solution;

import java.util.Enumeration;
import java.util.Vector;

public class RefactoredCustomer {

    private String name;
    private Vector rentals = new Vector();

    public RefactoredCustomer(String name) {
        this.name = name;
    }

    public void addRental(RefactoredRental aRental) {
        rentals.addElement(aRental);
    }

    public String getName() {
        return name;
    }

    public String statement() {

        double totalAmount = 0;
        int frequentRenterPoints = 0;
        Enumeration rentals = this.rentals.elements();
        String result = "RefactoredRental Record for " + getName() + "\n";

        //determine amounts for each line
        while (rentals.hasMoreElements()) {
            
            RefactoredRental each = (RefactoredRental) rentals.nextElement();
            // add frequent renter points

            frequentRenterPoints += each.calculateFrequentRenterPointForEachRental();

            //show figures for this RefactoredRental
            result += "\t" + each.getMovie().getTitle()+ "\t" +
                    String.valueOf(each.chargeForEachRentalData()) + "\n";
            totalAmount += each.chargeForEachRentalData();

        }

        //add footer lines
        result += "Amount owed is " + String.valueOf(calculateTotalCharge()) + "\n";
        result += "You earned " + String.valueOf(calculateTotalRenterPoints()) + " frequent renter points";
        return result;

    }

    public double calculateTotalCharge(){
        double totalCharge = 0.0;
        Enumeration rentals = this.rentals.elements();
        while(rentals.hasMoreElements()){
            RefactoredRental eachRental = (RefactoredRental) rentals.nextElement();
            totalCharge += eachRental.chargeForEachRentalData();
        }

        return totalCharge;
    }

    public int calculateTotalRenterPoints(){
        int totalRenterPoints = 0;
        Enumeration rentals = this.rentals.elements();
        while(rentals.hasMoreElements()){
            RefactoredRental eachRental = (RefactoredRental) rentals.nextElement();
            totalRenterPoints += eachRental.calculateFrequentRenterPointForEachRental();
        }

        return totalRenterPoints;
    }

}
