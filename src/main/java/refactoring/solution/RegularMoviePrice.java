package refactoring.solution;

public class RegularMoviePrice extends Price {
    @Override
    public int getFrequentRenterPoints(int daysRented) {
        return 1;
    }

    @Override
    public double getCharge(int daysRented) {
        double calculatedAmount = 2.0;
        if (daysRented > 2) calculatedAmount += (daysRented - 2) * 1.5;
        return calculatedAmount;
    }
}
