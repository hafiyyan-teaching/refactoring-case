package refactoring.solution;

public class RefactoredMovie {
    public static final int CHILDRENS = 2;
    public static final int NEW_RELEASE = 1;
    public static final int REGULAR = 0;

    private String title;
    private Price priceCode;

    public Price getPriceCode() {
        return priceCode;
    }

    public void setPriceCode(int arg){
        if(arg == CHILDRENS){
            priceCode = new ChihldrensMoviePrice();
        } else if (arg == NEW_RELEASE) {
            priceCode = new NewReleaseMoviePrice();
        } else if (arg == REGULAR) {
            priceCode = new RegularMoviePrice();
        }
    }

    public String getTitle() {
        return title;
    }

    public RefactoredMovie(String title, int priceCode) {
        this.title = title;
        setPriceCode(priceCode);
    }

    public double getCharge(int daysRented){
        return this.priceCode.getCharge(daysRented);
    }

    public int getFrequentRenterPoints(int daysRented){
        return this.priceCode.getFrequentRenterPoints(daysRented);
    }
}
