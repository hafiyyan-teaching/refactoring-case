package refactoring.solution;

public class ChihldrensMoviePrice extends Price {
    @Override
    public int getFrequentRenterPoints(int daysRented) {
        return 1;
    }

    @Override
    public double getCharge(int daysRented) {
        double calculatedAmount = 1.5;
        if (daysRented > 3)
            calculatedAmount += (daysRented - 3) * 1.5;
        return calculatedAmount;
    }


}
