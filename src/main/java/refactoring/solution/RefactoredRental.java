package refactoring.solution;

public class RefactoredRental {

    private RefactoredMovie movie;
    private int daysRented;

    public RefactoredRental(RefactoredMovie movie, int daysRented) {
        this.movie = movie;
        this.daysRented = daysRented;
    }

    public RefactoredMovie getMovie() {
        return movie;
    }

    public int getDaysRented() {
        return daysRented;
    }

    public double chargeForEachRentalData(){
        return getMovie().getCharge(this.daysRented);
    }

    public int calculateFrequentRenterPointForEachRental(){
        return getMovie().getFrequentRenterPoints(this.daysRented);
    }

}
