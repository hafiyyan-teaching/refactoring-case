package refactoring.solution;

public class NewReleaseMoviePrice extends Price {

    @Override
    public int getFrequentRenterPoints(int daysRented){
        return (daysRented > 1) ? 2 : 1;
    }

    @Override
    public double getCharge(int daysRented) {
        return daysRented * 3.0;
    }

}
