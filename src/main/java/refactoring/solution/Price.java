package refactoring.solution;

public abstract class Price {

    public abstract int getFrequentRenterPoints(int daysRented);
    public abstract double getCharge(int daysRented);
}
