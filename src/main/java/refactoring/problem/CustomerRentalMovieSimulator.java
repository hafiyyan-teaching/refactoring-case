package refactoring.problem;

public class CustomerRentalMovieSimulator {
    public static void main(String[] args){

        Movie aquaman = new Movie("Aquaman", Movie.NEW_RELEASE);
        Movie frozen2 = new Movie("Frozen 2", Movie.CHILDRENS);

        Rental aquamanRental = new Rental(aquaman, 3);
        Rental frozen2Rental = new Rental(frozen2, 2);

        Customer adam = new Customer("Adam");
        adam.addRental(aquamanRental);
        adam.addRental(frozen2Rental);

        System.out.println(adam.statement());
    }
}
