package refactoring.solution;

import static org.hamcrest.Matchers.samePropertyValuesAs;
import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class RefactoredRentalTest {

    private RefactoredMovie targetMovie;
    private RefactoredRental targetRental;
    private static final String titleTest = "Testing";
    private static final int priceCodeTest = RefactoredMovie.NEW_RELEASE;
    private static final int daysRentalTest = 2;

    @BeforeEach
    public void setUp(){
        targetMovie = new RefactoredMovie(titleTest, priceCodeTest);
        targetRental = new RefactoredRental(targetMovie, daysRentalTest);
    }

    @Test
    public void testGetMovie(){
        assertThat(targetMovie, samePropertyValuesAs(targetRental.getMovie()));
    }

    @Test
    public void testGetDaysRented(){
        assertEquals(targetRental.getDaysRented(), daysRentalTest);
    }

}
