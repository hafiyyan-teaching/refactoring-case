package refactoring.solution;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class RefactoredMovieTest {

    private RefactoredMovie targetMovie;
    private static final String titleTest = "Testing";
    private static final int priceCodeTest = RefactoredMovie.NEW_RELEASE;

    @BeforeEach
    public void setUp(){
        targetMovie = new RefactoredMovie(titleTest, priceCodeTest);
    }

    @Test
    public void testNewMovieGetPriceCode(){
        assertEquals(targetMovie.getPriceCode(), priceCodeTest);
    }

    @Test
    public void testNewMovieSetPriceCode(){
        targetMovie.setPriceCode(RefactoredMovie.CHILDRENS);
        assertEquals(targetMovie.getPriceCode(), RefactoredMovie.CHILDRENS);
    }

    @Test
    public void testNewMovieGetTitle(){
        assertEquals(targetMovie.getTitle(), titleTest);
    }

}
