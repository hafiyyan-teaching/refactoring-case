package refactoring.solution;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class RefactoredCustomerTest {

    private RefactoredMovie targetMovie;
    private RefactoredRental targetRental;
    private RefactoredCustomer targetCustomer;
    private static final String titleTest = "Testing";
    private static final int priceCodeTest = RefactoredMovie.NEW_RELEASE;
    private static final int daysRentalTest = 2;
    private static final String customerNameTest = "Adam";


    @BeforeEach
    public void setUp(){
        targetMovie = new RefactoredMovie(titleTest, priceCodeTest);
        targetRental = new RefactoredRental(targetMovie, daysRentalTest);
        targetCustomer = new RefactoredCustomer(customerNameTest);
    }

    @Test
    public void testGetCustomerName(){
        assertEquals(targetCustomer.getName(), customerNameTest);
    }

    @Test
    public void testStatement(){
        assertEquals(targetCustomer.statement(), "RefactoredRental Record for Adam\n" +
                "Amount owed is 0.0\n" +
                "You earned 0 frequent renter points");
    }

    @Test
    public void sophisticatedTestStatement(){
        RefactoredMovie aquaman = new RefactoredMovie("Aquaman", RefactoredMovie.NEW_RELEASE);
        RefactoredMovie frozen2 = new RefactoredMovie("Frozen 2", RefactoredMovie.CHILDRENS);

        RefactoredRental aquamanRental = new RefactoredRental(aquaman, 3);
        RefactoredRental frozen2Rental = new RefactoredRental(frozen2, 2);

        RefactoredCustomer adam = new RefactoredCustomer("Adam");
        adam.addRental(aquamanRental);
        adam.addRental(frozen2Rental);

        assertEquals(adam.statement(), "RefactoredRental Record for Adam\n" +
                "\tAquaman\t9.0\n" +
                "\tFrozen 2\t1.5\n" +
                "Amount owed is 10.5\n" +
                "You earned 3 frequent renter points");
    }
}
