package refactoring.problem;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class MovieTest {

    private Movie targetMovie;
    private static final String titleTest = "Testing";
    private static final int priceCodeTest = Movie.NEW_RELEASE;

    @BeforeEach
    public void setUp(){
        targetMovie = new Movie(titleTest, priceCodeTest);
    }

    @Test
    public void testNewMovieGetPriceCode(){
        assertEquals(targetMovie.getPriceCode(), priceCodeTest);
    }

    @Test
    public void testNewMovieSetPriceCode(){
        targetMovie.setPriceCode(Movie.CHILDRENS);
        assertEquals(targetMovie.getPriceCode(), Movie.CHILDRENS);
    }

    @Test
    public void testNewMovieGetTitle(){
        assertEquals(targetMovie.getTitle(), titleTest);
    }

}
