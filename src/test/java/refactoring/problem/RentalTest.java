package refactoring.problem;

import static org.hamcrest.Matchers.samePropertyValuesAs;
import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class RentalTest {

    private Movie targetMovie;
    private Rental targetRental;
    private static final String titleTest = "Testing";
    private static final int priceCodeTest = Movie.NEW_RELEASE;
    private static final int daysRentalTest = 2;

    @BeforeEach
    public void setUp(){
        targetMovie = new Movie(titleTest, priceCodeTest);
        targetRental = new Rental(targetMovie, daysRentalTest);
    }

    @Test
    public void testGetMovie(){
        assertThat(targetMovie, samePropertyValuesAs(targetRental.getMovie()));
    }

    @Test
    public void testGetDaysRented(){
        assertEquals(targetRental.getDaysRented(), daysRentalTest);
    }

}
