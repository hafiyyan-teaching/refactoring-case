package refactoring.problem;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class CustomerTest {

    private Movie targetMovie;
    private Rental targetRental;
    private Customer targetCustomer;
    private static final String titleTest = "Testing";
    private static final int priceCodeTest = Movie.NEW_RELEASE;
    private static final int daysRentalTest = 2;
    private static final String customerNameTest = "Adam";


    @BeforeEach
    public void setUp(){
        targetMovie = new Movie(titleTest, priceCodeTest);
        targetRental = new Rental(targetMovie, daysRentalTest);
        targetCustomer = new Customer(customerNameTest);
    }

    @Test
    public void testGetCustomerName(){
        assertEquals(targetCustomer.getName(), customerNameTest);
    }

    @Test
    public void testStatement(){
        assertEquals(targetCustomer.statement(), "Rental Record for Adam\n" +
                "Amount owed is 0.0\n" +
                "You earned 0 frequent renter points");
    }

    @Test
    public void sophisticatedTestStatement(){
        Movie aquaman = new Movie("Aquaman", Movie.NEW_RELEASE);
        Movie frozen2 = new Movie("Frozen 2", Movie.CHILDRENS);

        Rental aquamanRental = new Rental(aquaman, 3);
        Rental frozen2Rental = new Rental(frozen2, 2);

        Customer adam = new Customer("Adam");
        adam.addRental(aquamanRental);
        adam.addRental(frozen2Rental);

        assertEquals(adam.statement(), "Rental Record for Adam\n" +
                "\tAquaman\t9.0\n" +
                "\tFrozen 2\t1.5\n" +
                "Amount owed is 10.5\n" +
                "You earned 3 frequent renter points");
    }
}
